# OurUmbraco Event handlers



The repository will has a free gmail SMTP server setup based on https://gitlab.com/umbracohowto/OurUmbracoSMTP

```
{
	"Umbraco backoffice user" : {
		"username": "test@test.com",
		"password": "Mgj]O}7o{p"
	}
}
```

The following file shows various uses cases of hooking up into Umbraco backoofice events. It does some of the following:
- When a node of the "eventNode" documentType is created it prefils the email of the backoffice umbraco user as a default value
- When a node of the "eventNode" documentType is deleted an email is sent to email filled in the property "someEmail".
_This works only for version 8, if you want to know how this is done in version 9, please switch to the version-9 branch of this repository_

```
using System.Linq;
using System.Net.Mail;
using System.Text;
using Umbraco.Core;
using Umbraco.Core.Composing;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Core.Services.Implement;
using Umbraco.Web;
using Umbraco.Web.Editors;
using Umbraco.Web.Models.ContentEditing;
using Umbraco.Web.Security;

namespace UmbracoV8.Core
{
	[RuntimeLevel(MinLevel = RuntimeLevel.Run)]
	public class SubscribeToEditorModelEventsComposer : ComponentComposer<SubscribeToEditorModelEvents>
	{
		// This automatically adds the component to the Components collection of the Umbraco composition
	}
	public class SubscribeToEditorModelEvents : IComponent
	{
		private readonly IUmbracoContextFactory _umbracoContextFactory;

		public SubscribeToEditorModelEvents(IUmbracoContextFactory umbracoContextFactory)
		{
			_umbracoContextFactory = umbracoContextFactory;
		}

		public void Initialize()
		{
			EditorModelEventManager.SendingContentModel += EditorModelEventManager_SendingContentModel;
			ContentService.Trashing += ContentService_Trashing;
		}
		public static void FireEmailAndForget(string to, string title, string body, string[] CC = null)
		{
			var emailMessage = new MailMessage();
			emailMessage.To.Add(new MailAddress(to));
			if (CC != null)
			{
				foreach (var cc in CC)
				{
					emailMessage.CC.Add(new MailAddress(cc));
				}
			}
			emailMessage.Subject = title;
			emailMessage.Body = body;
			emailMessage.IsBodyHtml = true;
			emailMessage.BodyEncoding = Encoding.UTF8;
			emailMessage.HeadersEncoding = Encoding.UTF8;
			SmtpClient client = new SmtpClient();
			client.Send(emailMessage);
		}
		private void ContentService_Trashing(IContentService sender, MoveEventArgs<IContent> e)
		{
			var firstNodeBeingTrashed = e.MoveInfoCollection.FirstOrDefault(x => x.Entity.ContentType.Alias == "eventNode");
			if (firstNodeBeingTrashed != null)
			{
				var node = Umbraco.Web.Composing.Current.UmbracoHelper.Content(firstNodeBeingTrashed.Entity.Id);
				var email = node.Value<string>("someEmail");
				var url = node.Url();
				FireEmailAndForget(email, "Trashing your content", $"Node with ID:{node.Id} is being trashed, so the Url '{url}' might stop working");
			}
		 }

		public void Terminate()
		{
			// Unsubscribe during shutdown
			EditorModelEventManager.SendingContentModel -= EditorModelEventManager_SendingContentModel;
			ContentService.Trashing -= ContentService_Trashing;
		}

		private void EditorModelEventManager_SendingContentModel(System.Web.Http.Filters.HttpActionExecutedContext sender, EditorModelEventArgs<Umbraco.Web.Models.ContentEditing.ContentItemDisplay> e)
		{
			// Set a default value for a NewsArticle's PublishDate property, the editor can override this, but we will suggest it should be today's date
			if (e.Model.ContentTypeAlias == "eventNode")
			{
				//access the property you want to pre-populate
				//in V8 each content item can have 'variations' - each variation is represented by the `ContentVariantDisplay` class.
				//if your site uses variants, then you need to decide whether to set the default value for all variants or a specific variant
				// eg. set by variant name:
				// var variant = e.Model.Variants.FirstOrDefault(f => f.Name == "specificVariantName");
				// OR loop through all the variants:
				foreach (var variant in e.Model.Variants)
				{
					//check if variant is a 'new variant' - we only want to set the default value when the content item is first created
					if (variant.State == ContentSavedState.NotCreated)
					{
						// each variant has an IEnumerable of 'Tabs' (property groupings) and each of these contain an IEnumerable of `ContentPropertyDisplay` properties
						var someEmailProperty = variant.Tabs.SelectMany(f => f.Properties).FirstOrDefault(f => f.Alias.InvariantEquals("someEmail"));
						if (someEmailProperty != null)
						{
							using (UmbracoContextReference umbracoContextReference = _umbracoContextFactory.EnsureUmbracoContext())
							{
								var userTicket = umbracoContextReference.UmbracoContext.HttpContext.GetUmbracoAuthTicket();
								if (userTicket != null)
								{
									someEmailProperty.Value = Current.Services.UserService.GetByUsername(userTicket.Identity.Name).Email;
								}
							}
						}
					}
				}
			}
		}
	}
}
```
