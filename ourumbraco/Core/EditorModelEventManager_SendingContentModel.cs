﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Umbraco.Cms.Core;
using Umbraco.Cms.Core.Events;
using Umbraco.Cms.Core.Models.ContentEditing;
using Umbraco.Cms.Core.Models.Membership;
using Umbraco.Cms.Core.Notifications;
using Umbraco.Cms.Core.Security;
using Umbraco.Cms.Core.Web;
using Umbraco.Extensions;

namespace ourumbraco.Core
{
    public class EditorModelEventManager_SendingContentModel : INotificationHandler<SendingContentNotification>
    {
        private readonly IUmbracoContextFactory _umbracoContextFactory;
        private readonly IBackOfficeSecurityAccessor _backOfficeSecurityAccessor;

        public EditorModelEventManager_SendingContentModel(IUmbracoContextFactory umbracoContextFactory, IBackOfficeSecurityAccessor backOfficeSecurityAccessor)
        {
            _umbracoContextFactory = umbracoContextFactory;
            _backOfficeSecurityAccessor = backOfficeSecurityAccessor;
        }

        public void Handle(SendingContentNotification notification)
        {
            // Set the default value for the email property to be the current user email, the editors can then overwrite if they please
            if(notification.Content.ContentTypeAlias.Equals("eventNode"))
            {
                //access the property you want to pre-populate
                //in V8 each content item can have 'variations' - each variation is represented by the `ContentVariantDisplay` class.
                //if your site uses variants, then you need to decide whether to set the default value for all variants or a specific variant
                // eg. set by variant name:
                // var variant = e.Model.Variants.FirstOrDefault(f => f.Name == "specificVariantName");
                // OR loop through all the variants:
                foreach (var variant in notification.Content.Variants)
                {
                    //check if variant is a 'new variant' - we only want to set the default value when the content item is first created
                    if (variant.State == ContentSavedState.NotCreated)
                    {
                        // each variant has an IEnumerable of 'Tabs' (property groupings) and each of these contain an IEnumerable of `ContentPropertyDisplay` properties
                        var someEmailProperty = variant.Tabs.SelectMany(f => f.Properties).FirstOrDefault(f => f.Alias.InvariantEquals("someEmail"));
                        if (someEmailProperty != null)
                        {
                            IUser currentUser = _backOfficeSecurityAccessor.BackOfficeSecurity.CurrentUser;
                            if (currentUser != null)
                            {
                                someEmailProperty.Value = currentUser.Email;
                            }
                        }
                    }
                }
            }
        }
    }
}

