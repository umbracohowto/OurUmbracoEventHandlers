﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MimeKit;
using Umbraco.Cms.Core;
using Umbraco.Cms.Core.Configuration.Models;
using Umbraco.Cms.Core.Events;
using Umbraco.Cms.Core.Models.PublishedContent;
using Umbraco.Cms.Core.Notifications;
using Umbraco.Cms.Core.Scoping;
using Umbraco.Cms.Core.Web;
using Umbraco.Cms.Infrastructure;
using Umbraco.Cms.Web.Common.UmbracoContext;
using Umbraco.Extensions;
using SmtpClient = MailKit.Net.Smtp.SmtpClient;


namespace ourumbraco.Core
{
    public class SubscribeToMovingToRecycleBinNotification : INotificationHandler<ContentMovingToRecycleBinNotification>
    {
        private readonly IUmbracoContextFactory _umbracoContext;
        private readonly IScopeProvider _scopeProvider;
        private readonly GlobalSettings _globalSettings;
        private readonly ILogger<SubscribeToMovingToRecycleBinNotification> _logger;

        public SubscribeToMovingToRecycleBinNotification(IUmbracoContextFactory umbracoContextFactory, IScopeProvider scopeProvider, IOptions<GlobalSettings> globalSettings, ILogger<SubscribeToMovingToRecycleBinNotification> logger)
        {
            _umbracoContext = umbracoContextFactory;
            _scopeProvider = scopeProvider;
            _logger = logger;
            _globalSettings = globalSettings.Value;
        }

        public void FireEmailAndForget(string to, string title, string body, string[] CC = null)
        {
            // Using the internal Umbraco email sender as reference for this task - - https://github.com/umbraco/Umbraco-CMS/blob/v9/dev/src/Umbraco.Infrastructure/Mail/EmailSender.cs
            // Normal SmtpClient is obsolete - https://docs.microsoft.com/en-us/dotnet/api/system.net.mail.smtpclient?view=net-5.0

            if (_globalSettings.IsSmtpServerConfigured == false)
            {
                _logger.LogDebug("Could not send email for {Subject}. It was not handled by a notification handler and there is no SMTP configured.", to);
                return;
            }

            using var client = new SmtpClient();

             client.Connect(_globalSettings.Smtp.Host,
                _globalSettings.Smtp.Port,
                (MailKit.Security.SecureSocketOptions)(int)_globalSettings.Smtp.SecureSocketOptions);

            if (!(_globalSettings.Smtp.Username is null && _globalSettings.Smtp.Password is null))
            {
                 client.Authenticate(_globalSettings.Smtp.Username, _globalSettings.Smtp.Password);
            }

            var emailMessage = new MimeMessage();
            emailMessage.To.Add(MailboxAddress.Parse(to));
            emailMessage.Sender = MailboxAddress.Parse(_globalSettings.Smtp.From);

            if (CC != null)
            {
                foreach (var cc in CC)
                {
                    emailMessage.Cc.Add(new MailboxAddress(cc));
                }
            }
            emailMessage.Subject = title;
            emailMessage.Body = new TextPart("plain")
            {
                Text = body
            };
            
            client.Send(emailMessage);
        }

        // Is runtimeState necessary?
        // if (_runtimeState.Level != RuntimeLevel.Run)
        // {
        //   return;
        // }
        public void Handle(ContentMovingToRecycleBinNotification notification)
        {
            var firstNodeBeingTrashed = notification.MoveInfoCollection.FirstOrDefault(x => x.Entity.ContentType.Alias == "eventNode");
            if (firstNodeBeingTrashed != null)
            {
                var node = firstNodeBeingTrashed.Entity;
                var url = "";

                using (_scopeProvider. CreateScope(autoComplete: true))
                {
                    using (UmbracoContextReference contextReference = _umbracoContext.EnsureUmbracoContext())
                    {
                        IUmbracoContext context = contextReference.UmbracoContext;
                        IPublishedContent content = context.Content.GetById(node.Id);
                        // An unpublished node will contain no URL
                        if (node.Published)
                        {
                            url = content.Url();
                        }
                    }
                }
                var email = node.GetValue<string>("someEmail");
                
                FireEmailAndForget(email, "Trashing your content", $"Node with ID:{node.Id} is being trashed, so the Url '{url}' might stop working");
            }
        }
    }
}
